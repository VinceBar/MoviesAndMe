import React from 'react'
import { StyleSheet, View, Animated, Easing } from 'react-native'

class Test extends React.Component {

  constructor(props) {
   super(props)
   this.state = {
     topPosition: new Animated.Value(0)
   }
 }

 render() {
   return (
     <View style={styles.main_container}>
       <Animated.View style={[styles.animation_view, { top: this.state.topPosition }]}>
       </Animated.View>
     </View>
   )
 }

 componentDidMount() {

    Animated.timing(

      this.state.topPosition,

      {

        toValue: 100,

        duration: 3000, // Le temps est en milliseconds ici (3000ms = 3sec)

        easing: Easing.linear,

      }

    ).start() // N'oubliez pas de lancer votre animation avec la fonction start()

}

}
